#!/usr/bin/perl

use warnings;
use strict;

my %data;

my $line;
while ($line = <STDIN>) {
    chomp $line;
    my @parts = split /\s/, $line;
    my $x;
    my $y;
    if ($parts[0] eq "V3") {
        $x = $parts[3];
        $y = $parts[4];
    }
    else{
        $x = $parts[0];
        $y = $parts[1];
    }
    $x =~ s/\(//;
    $x =~ s/\)//;
    $y =~ s/\(//;
    $y =~ s/\)//;
    push @{ $data{$x} }, $y;
}

my $key;
for $key (sort { $a <=> $b } (keys %data)) {
    my @dat = @{ $data{$key} };
    my $sum = 0;
    for (@dat) { $sum += $_; }
    my $avg = $sum / (scalar(@dat));
    my $l = scalar(@dat);
    print "$key $avg\n";
}
    
