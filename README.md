# Monaco - A Monte Carlo Solver

Monaco is a "generic" Monte Carlo solver. It should in principle work for a vastly generalizable set of systems, from an Ising model to some weird n-dimensional lattice with 5 dimensional complex spinors on each site. Right now, however, all of the useful functions that we feed to the Metropolis solver are based on an O(3) Heisenberg model, so---without a little tinkering on the users part---Monaco is in effect an O(3) Monte Carlo solver.

The low level manipulations are covered up with a small, embedded DSL that lets you do things like smoothly manipulate temperature and field, while installing and running "observers" that collect different kinds of data at different points in the simulation.

Currently, the base algorithm uses Metropolis, with a tangent vector perturbation for the spins whose magnitude is adpatively tuned to reach a 50% acceptance ratio of MC steps. Unlike a discrete simulation where it's advantageous to have a near 100% acceptance ratio (so as to sample phase space faster), the trouble with our system is that moves with 100% acceptance ratio are typically extremely small, so although they are often accepted they also only trace out a small neighorhood of the smooth phase space manifold. 

## How to run:

To run it, make sure you have [stack](https://docs.haskellstack.org/en/stable/README/) installed. Then run run `stack setup`, and then `stack build`, in the project's root directory. Assuming the compilation goes without error, you can run the simulator with `stack exec monaco-exe`. To give command line options, run `stack exec monaco-exe -- -option1 -option2`. It's probably worth running it with the `--help` flag first.

Monaco actually complies into LLVM. You need to ensure that LLVM version 3.7 is installed, and that the appropriate LLVM tools are on your path. On MacOS with Homebrew installed, just run `brew install llvm@3.7`.

## General outline/guide to the code:

The "main" executable is in app/Main.hs. The function `main :: IO ()` is what gets called when the program runs.

Everything else is in src/\*.hs:

  * Analysis.hs was some stuff I once tried to write to automatically extract the width of the hysteresis loop, but it wasn't really necessary and the code is now deprecated. It's not even included in the compilation sequence.
  * Energy.hs contains the various interaction terms: exchange, DMI, anisotropy, applied fields, etc. The code was originally written for analysis of magnetic multilayers, which is why exchange parameters throughout the code usually appear in pairs: the intra- and inter-layer coupling. In the monolayer case, we (obviously) only use the intralayer coupling. Energy.hs has some vector math, and to understand the operators you should go look up the [linear library](https://hackage.haskell.org/package/linear).
  * Interpreter.hs establishes a DSL for specificing how a simulation is built. It provides primitive operations like annealing, hysteresis, et cetera that can be composed to build a complete simulation routine in Main.hs. It also provides functions for installing observers.
  * Lattice.hs establishes the mathematics of neighbor-finding, boundary conditions, and lattices. The most important functions it exports are baseNeighborContraction and baseTargetNeighborContraction used in Energy.hs. I wrote this stuff early on and generalized the types to hell, and quite frankly writing a more specialized version of some of this stuff for the concrete O(3) model on a square lattice would likely speed things up.
  * Lib.hs is boilerplate garbage, it should be deleted.
  * MonteCarlo.hs contains the Metropolis algorithm and the support code required to run it.
  * Observations.hs provides "observers" that record data at certain stages of the simulation specified in MonteCarlo.hs. I suspect that the way I use these observers is highly inefficient and a serious performance cost, but not enough of a cost for me to bother rewriting it all.
  * Options.hs defines the command line arguments.
  * Simulation.hs defines the actually parameter sweeps underling the commands provided in Interpreter.hs, as well as a function for building the Parameter state that threads through the State monad around the actual simulation.
  * Types.hs is packed to the brim with types and instance declarations, and a re-export of microlens.
