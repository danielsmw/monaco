{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ConstraintKinds #-}
module Main where

import Types
import Simulation (buildParams)
import Observations
import Options (ProgOpts (..), optionHandler, execParser)
import Interpreter
       (anneal, hysteresis, registerObserver, unregisterObserver,
        activateObserver, deactivateObserver, interpret, ObserverType(..),
        Iterations(..), LowHighDelta(..))

import Control.Monad.ST
import Control.Monad.State.Strict

import Data.Foldable (sequence_)
import Data.Vector.Unboxed as V

import System.Random.MWC
  
main :: IO ()
main = do
  -- Get command line options
  opts <- execParser optionHandler

  -- Run the experiment
  ds <-
    withSystemRandom . asGenST $
    skyrmionNumberExperiment
      [width opts, height opts]
      (0.0, exchange opts)
      (eaa opts)
      (dmi opts)
      opts

  -- Process observed data
  let dataset =
        (\obd -> (unboxObserverID $ obd ^. observerID, obd ^. collectedData)) <$>
        (ds ^. observations)
      thermalDataset =
        (\obd -> (unboxObserverID $ obd ^. observerID, obd ^. collectedData)) <$>
        (ds ^. thermalObservations)

  -- Write that data to disk
  putStrLn "handling sampling data..." >>
    (sequence_ $ uncurry (datasetHandler (outputDirectory opts)) <$> dataset)
  putStrLn "handling thermal data..." >>
    (sequence_ $ uncurry (datasetHandler (outputDirectory opts)) <$> thermalDataset)
   
{-| Given dimensionality, exchange, EAA, and DMI, as well as the rest of
    the options for fun, run the experiment detailed in
    'singleLoopHysteresisSkyrmion'. -}
skyrmionNumberExperiment
  :: [Int]
  -> (Double, Double)
  -> Double
  -> Double
  -> ProgOpts
  -> Gen (PrimState (ST s))
  -> ST s (Parameters S2 Double Double S2 (ST s))
skyrmionNumberExperiment sizes iixs k d opts g = do
  let ti = initialTemp opts
      tf = finalTemp opts
      dt = deltaTemp opts
      n = iterationsPerMC opts
      b0 = maxB opts
      dB = deltaB opts
  ps <- buildParams g (V.fromList sizes) iixs k d (Temperature tf)
  singleLoopHysteresisSkyrmion sizes n (b0, dB) (ti, dt, tf) ps
  --singleLoopHysteresisSkyrmion sizes 200 g (3.0, 3.0) (10.0, 5.0) ps
  --singleSkyrmion sizes 10000 g (60.0, 2.5) (5.0, 0.5) ps

{-| Run an experiment that involves annealing a random system from an
    initial to final temperature, and then circle a hysteresis loop on
    the annealed system. -}
singleLoopHysteresisSkyrmion
  :: [Int] -> Int
  -> (Double, Double)
  -> (Double, Double, Double)
  -> Parameters S2 Double Double S2 (ST s)
  -> ST s (Parameters S2 Double Double S2 (ST s))
singleLoopHysteresisSkyrmion sizes' n (bmax, db) (tmax, dt, tmin) ps = do
  let sizes = V.fromList sizes'
  let simulation =
        interpret $
        do -- First, we turn on some observers:
           registerObserver
             Thermal
             (magnetizationObserver
                (const True)
                TemperatureIV
                (V3 0.0 0.0 1.0)
                (ObserverID "M(T)"))
           registerObserver
             Thermal
             (skyrmionObserver TemperatureIV (ObserverID "Q(T)"))

           -- Then anneal the system
           anneal
             (Iterations n)
             (LowHighDelta
                ( Temperature tmax
                , Temperature tmin
                , Temperature (negate dt)))

           -- Now that annealing is done, deactivate those observers
           -- and activate some new ones instead
           deactivateObserver Thermal (ObserverID "M(T)")
           deactivateObserver Thermal (ObserverID "Q(T)")
           registerObserver
             Thermal
             (magnetizationObserver
                (const True)
                (InteractionIV "B_z")
                (V3 0.0 0.0 1.0)
                (ObserverID "M(B-)"))
           registerObserver
             Thermal
             (skyrmionObserver (InteractionIV "B_z") (ObserverID "Q(B-)"))
           registerObserver Sample
             (imageChargeObserver
                (\x y -> (sizes ! 0) * y + x)
                (hslFromS2_RGB8_BW)
                (InteractionIV "B_z")
                (sizes ! 0)
                (sizes ! 1)
                (ObserverID "A(B-)"))
           registerObserver Sample
             (imageObserver
                (\x y -> (sizes ! 0) * y + x)
                (hslFromS2_RGB8)
                (InteractionIV "B_z")
                (sizes ! 0)
                (sizes ! 1)
                (ObserverID "S(B-)"))

           -- Then run down (leftward) the hysteresis loop
           hysteresis
             (Iterations n)
             (V3 0.0 0.0 1.0)
             (LowHighDelta (bmax, negate bmax, negate db))

           -- Switch observers again
           deactivateObserver Thermal (ObserverID "M(B-)")
           deactivateObserver Thermal (ObserverID "Q(B-)")
           deactivateObserver Thermal (ObserverID "A(B-)")
           deactivateObserver Thermal (ObserverID "S(B-)")
           registerObserver Sample
             (imageObserver
                (\x y -> (sizes ! 0) * y + x)
                (hslFromS2_RGB8)
                (InteractionIV "B_z")
                (sizes ! 0)
                (sizes ! 1)
                (ObserverID "S(B+)"))
           registerObserver
             Thermal
             (magnetizationObserver
                (const True)
                (InteractionIV "B_z")
                (V3 0.0 0.0 1.0)
                (ObserverID "M(B+)"))
           registerObserver
             Thermal
             (skyrmionObserver (InteractionIV "B_z") (ObserverID "Q(B+)"))
           registerObserver Sample
             (imageChargeObserver
                (\x y -> (sizes ! 0) * y + x)
                (hslFromS2_RGB8_BW)
                (InteractionIV "B_z")
                (sizes ! 0)
                (sizes ! 1)
                (ObserverID "A(B+)"))

           -- Run back up the hysteresis loop
           hysteresis
             (Iterations n)
             (V3 0.0 0.0 1.0)
             (LowHighDelta (negate bmax, bmax, db))

  -- Actually do the computation we just constructed:
  execStateT simulation ps
