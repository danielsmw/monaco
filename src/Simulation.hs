{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE BangPatterns, ScopedTypeVariables #-}
module Simulation
  ( interactionScan
  , annealSweep
  , buildParams
  ) where

import Control.DeepSeq
import Data.Ratio
import Debug.Trace
import Data.Map (insert, delete, fromList, empty)
import Data.Vector.Unboxed as V
import qualified Data.Vector as BV
import Linear.Vector
import Linear.V3 (V3(..), cross)
import Types
import Lattice (hypertorus, cubicTorus)
import Energy
import MonteCarlo
       (walkSpinHector, boltzmannAccept, randomSpherical)
import Observations

{-| Scan a new interaction, given on the second argument, through a
    vector of values given on the first argument, running the provided
    MC routine for each value of the field. -}
interactionScan
  :: (Show base, Show b, Show field, PrimMonad m)
  => BV.Vector Double
  -> (Double -> Interaction field base p b m)
  -> Simulation field base p b m ()
  -> Simulation field base p b m ()
interactionScan bs gen mcRoutine =
  BV.mapM_
    (\b -> do
       let i = gen b
       metropolis . interactions %= insert (name i) i
       traceShow
         ("running at " Prelude.++ (name i) Prelude.++ " = " Prelude.++
          (show $ param i))
         mcRoutine
       metropolis . interactions %= delete (name i))
    bs

{-| Just like 'interactionScan', but for temperature. It's a separate
    function because we treat temperature differently from, say, a
    magnetic field. Unlike a magnetic field, you can't have a MC
    simulation without temperature and you also can't add more than one
    "temperature field". -}
annealSweep
  :: (PrimMonad m, base ~ Double, field ~ V3 Double)
  => [Temperature]
  -> Simulation field base p b m ()
  -> Simulation field base p b m ()
annealSweep ts mcRoutine = do
  metropolis . interactions %= insert (name b) b
  Prelude.mapM_
    (\(Temperature t) -> do
       metropolis . temperature .= t
       traceShow ("running at T = " Prelude.++ (show $ t)) mcRoutine)
    ts
  metropolis . interactions %= delete (name b)
  where
    b =
      Interaction
      { name = "bias_internal_17bfZXBVD213"
      , param = FieldParameter (V3 0 0 0.1 :: V3 Double)
      , operator = \(FieldParameter s) -> appliedField s
      }

{-| A skyrmion configuration. One may consider using this, instead of a
  random configuration, for the initial conditions. -}
skyrmionConfig sizes =
  (Functional $
   \i ->
      let x =
            (fromIntegral $ i `rem` (sizes ! 0)) -
            (fromIntegral $ sizes ! 0) / 2
          y =
            (fromIntegral $ i `quot` (sizes ! 0)) -
            (fromIntegral $ sizes ! 1) / 2
          t = atan2 y x
          r = sqrt $ x ^ 2 + y ^ 2
          r0 = (fromIntegral (sizes ! 0)) / 5
          denom = r ^ 2 + r0 ^ 2
          sZ = cos $ (-2) * (atan $ exp ((r-r0)/4))
          sX = (sqrt $ 1 - sZ^2) * cos t
          sY = (sqrt $ 1 - sZ^2) * sin t
      in let v = V3 sX sY sZ in traceShow v v)

{-| Construct a 'Parameters' object for the sort of standard, J+K+D O(3)
    simulations we actually care about. -}
buildParams
  :: forall m . PrimMonad m
  => Gen (PrimState m)
  -> V.Vector Int
  -> (Double, Double)
  -> Double
  -> Double
  -> Temperature
  -> m (Parameters S2 Double MaximumTangent S2 m)
buildParams g sizes (_, intraJ) eaa dmi (Temperature t) = do
  ld <- hypertorus g (cubicTorus sizes) (Randomized $ \g' _ -> randomSpherical g') sizes
  --ld <- hypertorus g (cubicTorus sizes) (skyrmionConfig sizes) sizes
  return $ Parameters ld g metroParams Data.Map.empty Data.Map.empty
--dmi = cross (V3 0.0 0.0 0.5) :: S2 -> S2
  where
    metroParams =
      MetropolisParams
      { _interactions =
        Data.Map.fromList $
        (\i -> (name i, i)) <$>
        [ Interaction
          { name = "exchange"
          , param = BaseParameter intraJ
          , operator = \(BaseParameter j) -> uniformExchange j
          }
          {-Interaction
          { name = "exchange"
          , param = PairParameter (BaseParameter interJ) (BaseParameter intraJ)
          , operator =
            \(PairParameter (BaseParameter inter) (BaseParameter intra)) ->
               interIntraExchange inter intra
          }-}
        , Interaction
          { name = "anisotropy"
          , param = BaseParameter eaa
          , operator = \(BaseParameter k) -> uniaxialAnisotropyZ k
          }
        , Interaction
          { name = "dmi"
          , param = BaseParameter dmi
          , operator = \(BaseParameter d) -> antisymmetricExchangeNN (\r -> d *^ r) }
        ]
      --[uniformExchange (-1.0), antisymmetricExchangeNN dmi]
      , _acceptanceCriteria = boltzmannAccept
      , _perturbation = ParameterizedPerturbation walkSpinHector
      , _temperature = t
      , _maximumIterations = 50
      , _thermalSampling = Just (1 % 200)
      }
