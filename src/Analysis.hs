{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
module Analysis where

import qualified Data.Map as Map
import Data.Ext
import Data.Range
import Data.Geometry.Point
import Data.Geometry.LineSegment
import Algorithms.Geometry.LineSegmentIntersection.BentleyOttmann

data HysteresisLoop = HysteresisLoop [LineSegment 2 () Double]

asHysteresisLoop :: [(Double, Double)] -> HysteresisLoop
asHysteresisLoop dat = HysteresisLoop segs
  where
    pts = map (uncurry point2) dat
    segs =
      zipWith
        (\p1 p2 -> LineSegment (Open $ p1 :+ ()) (Closed $ p2 :+ ()))
        pts
        (tail $ cycle pts)

coercivity :: HysteresisLoop -> Double
coercivity (HysteresisLoop segs) =
  maximum $ map greatestInterval intersects
  where
    intersects = map (Map.keys . intersections . (: segs)) sweepLines
    greatestInterval [] = 0.0
    greatestInterval (_:[]) = 0.0
    greatestInterval (p0:p1:ps) = max (euclideanDist p0 p1) (greatestInterval (p1:ps))
    sweepLines =
      [ LineSegment
         (Closed $ point2 (-1000.0) y :+ ())
         (Closed $ point2 (1000.0) y :+ ())
      | y <- [(-400),(-390) .. 400] ]
