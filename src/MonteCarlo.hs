{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE BangPatterns #-}

module MonteCarlo
  ( metropolisSequence
  , metroStep
  , boltzmannAccept
  , walkSpinHector
  , randomSpherical
  ) where

import Types
import Observations

import Control.Monad (replicateM_, when)
import Control.DeepSeq (NFData ())

import Data.Ratio
import Data.Map (elems)
import Data.Maybe
import Data.Vector.Unboxed as V
import qualified Data.Vector as BV
import Data.Vector.Unboxed.Mutable as MV

import Debug.Trace

import Linear.Vector
import Linear.V3 (V3(..))
import Linear.Metric (normalize, project)

import System.Random.MWC

type AcceptanceRatio = Ratio Int
  
{-# SPECIALISE adaptiveMetropolisPerturbation ::
                 PrimMonad m =>
                 (Double -> UnboxedPerturbation (V3 Double) Double Double S2 m)
                   ->
                   Double ->
                     Int ->
                       AcceptanceRatio ->
                         (Int, Int) ->
                           Simulation (V3 Double) Double Double S2 m
                             (UnboxedPerturbation (V3 Double) Double Double S2 m)
               #-}
adaptiveMetropolisPerturbation
  :: (Fractional p, Show p
     ,Unbox k
     ,Unbox (target k)
     ,target k ~ field
     ,PrimMonad m
     ,Num k)
  => (p -> UnboxedPerturbation field k p b m)
  -> p
  -> Int
  -> AcceptanceRatio
  -> (Int, Int)
  -> Simulation field k p b m (UnboxedPerturbation field k p b m)
adaptiveMetropolisPerturbation f p0 t epsilon (n, 0) = do
  -- If we get to this base case and the acceptance ratio has not
  -- converged, it's time to just give up.
  let perturb' = f p0
  ratio <- queryAcceptanceRatio perturb' t
  case abs (ratio - 1 % 2) < epsilon of
    True -> return $ f p0
    False ->
      trace
        (Prelude.concat
           [ "Warning: after "
           , show n
           , " iterations, the acceptance ratio (currently "
           , show ratio
           , ") has not\n\tconverged to (1 % 2). Pressing on with current perturbation value of\n\t"
           , show p0
           , ". This probably tindicates that the system is above its\n\tordering temperature."
           ]) $
      return $ f p0
adaptiveMetropolisPerturbation f p0 t epsilon (n, j) = do
  let perturb' = f p0
  ratio <- queryAcceptanceRatio perturb' t
  let ratio' =
        case (ratio == 0 % 1, ratio == 1 % 1) of
          (True, _) -> 1 % t
          (_, True) -> (pred t) % t
          _ -> ratio
  case abs (ratio' - 1 % 2) < epsilon of
    True -> return $ f p0
    False ->
      adaptiveMetropolisPerturbation
        f
        (2 * p0 * (fromIntegral $ numerator ratio') /
         ((fromIntegral $ denominator ratio')))
        t
        epsilon
        (n, pred j)

{-# SPECIALIZE queryAcceptanceRatio :: PrimMonad m => UnboxedPerturbation (V3 Double) Double Double S2 m -> Int -> Simulation (V3 Double) Double Double S2 m AcceptanceRatio #-}
queryAcceptanceRatio
  :: (Unbox k, Unbox (target k), target k ~ field, PrimMonad m, Num k)
  => UnboxedPerturbation field k p b m -> Int -> Simulation field k p b m AcceptanceRatio
queryAcceptanceRatio perturb t = do
  accept <- use $ metropolis . acceptanceCriteria
  interacts <- use $ metropolis . interactions
  (Lattice v) <- use $ lattice . dataVector
  g <- use rng
  n <- use $ lattice . totalNodes
  bs <-
    V.replicateM t $
    metroStep accept perturb g v n $
    unboxInteraction <$> (BV.fromList $ elems interacts)
  return $ (count bs % t)
  where
    count =
      V.foldl'
        (\a b ->
            if b
              then succ a
              else a)
        0

{-# SPECIALISE metropolisSequence ::
                 PrimMonad m => Int -> Simulation (V3 Double) Double MaximumTangent S2 m ()
               #-}
{-| This is where everything happens. Assuming the user has specified as
    such, we first try to adaptively retrieve a perturbation value
    that gets us near an 50% acceptance ratio. Then we run a bunch of
    'metroStep' calls. Depending on whether thermal samping has been
    requested (which, stupidly, is a separate option from whether or
    not there actually exist any 'Thermal'-type 'ObservationDispatch'
    objects in the observer queues), we may run these in batches and
    stop to take measurements after each batch. Then we run 'Sample'
    type observers to finish. -}
metropolisSequence
  :: (Fractional p, Show p
     ,Unbox k
     ,Unbox (target k)
     ,target k ~ field
     ,PrimMonad m
     ,Num k
     ,NFData b, NFData k, NFData field
     ,Show (Observation field k p b m))
  => Int -> Simulation field k p b m ()
metropolisSequence t = do
  accept <- use $ metropolis . acceptanceCriteria
  perturb' <- use $ metropolis . perturbation
  maxIterations <- use $ metropolis . maximumIterations
  thermal <- use $ metropolis . thermalSampling
  perturb <-
    case perturb' of
      Perturbation p -> return $ p
      ParameterizedPerturbation pp ->
        adaptiveMetropolisPerturbation
          pp
          1.0
          400
          (3 % 100)
          (maxIterations, maxIterations)
  interacts <- use $ metropolis . interactions
  (Lattice v) <- use $ lattice . dataVector
  g <- use rng
  n <- use $ lattice . totalNodes
  when (isNothing thermal) $
    do replicateM_ t $
         metroStep accept perturb g v n $
         fmap unboxInteraction $ BV.fromList $ elems interacts
  when (isJust thermal) $
    let r = fromJust thermal
        m = (fromIntegral $ numerator r) :: Double
        d = (fromIntegral $ denominator r) :: Double
    in do replicateM_ (round $ (0.1 * (fromIntegral t) :: Double)) $
          -- First do 10% to equilibrate. This should be replaced with
          -- a feature that tries to wait the calculated/observed
          -- correlation time instead.
            metroStep accept perturb g v n $
            fmap unboxInteraction $ BV.fromList $ elems interacts
          replicateM_ (round $ d / m) $
            do replicateM_ (round $ m * (fromIntegral t) / d) $
                 metroStep accept perturb g v n $
                 fmap unboxInteraction $ BV.fromList $ elems interacts
               runThermalObservations 
  runObservations
  return ()

{-# INLINE metroStep #-}
{-# SPECIALIZE metroStep :: PrimMonad m => AcceptanceCriteria S2 Double
  Double S2 m -> UnboxedPerturbation S2 Double Double S2 m -> Gen (PrimState
  m) -> MV.MVector (PrimState m) S2 -> Int -> BV.Vector
  (UnboxedInteraction S2 Double Double S2 m) -> Simulation S2 Double
  Double S2 m Bool #-}
metroStep
  :: (Unbox k, Unbox (target k), target k ~ field, PrimMonad m, Num k)
  => AcceptanceCriteria field k p b m
  -> UnboxedPerturbation field k p b m
  -> Gen (PrimState m)
  -> MV.MVector (PrimState m) field
  -> Int
  -> BV.Vector (UnboxedInteraction field k p b m)
  -> Simulation field k p b m Bool
metroStep accept perturb g v n es = do
  !j <- uniformR (0, pred n) g
  !s <- v `MV.unsafeRead` j
  !s' <- perturb s
  !e0 <- BV.sum <$> (($ (j, s)) `BV.mapM` es)
  !e1 <- BV.sum <$> (($ (j, s')) `BV.mapM` es)
  !accepted <- accept g $ e1 - e0
  when accepted (unsafeWrite v j s')
  return accepted

{-# INLINE boltzmannAccept #-}
{-# SPECIALIZE boltzmannAccept :: PrimMonad m => Gen (PrimState m) -> Double -> Simulation (V3 Double) Double MaximumTangent S2 m Bool #-}
boltzmannAccept
  :: PrimMonad m
  => Gen (PrimState m) -> Double -> Simulation field base p b m Bool
boltzmannAccept !g !dE = do
  !p <- uniform g
  !kT <- use (metropolis.temperature)
  return (dE < 0 || p < exp (-dE / kT)) --0.0862 meV = k_B * 1 K

{-# INLINE walkSpinHector #-}
{-# SPECIALIZE walkSpinHector :: PrimMonad m => MaximumTangent -> S2 -> Simulation S2 Double MaximumTangent S2 m S2 #-}
walkSpinHector
  :: PrimMonad m
  => MaximumTangent -> S2 -> Simulation S2 Double MaximumTangent b m S2
walkSpinHector !m !s = do
  !g <- use rng
  !u <- randomSpherical g
  !w <- uniformR (0.0, m) g
  let !ds = w *^ (normalize $ u ^-^ project s u)
  return $ normalize $ s ^+^ ds

{-# INLINE randomSpherical #-}
randomSpherical :: PrimMonad m => Gen (PrimState m) -> m S2
randomSpherical !g = do
  !u <- uniform g
  !v <- uniform g
  let !t = acos $ 2 * u - 1
      !p = 2 * pi * v
  return $ V3 (cos p * sin t) (sin p * sin t) (cos t)
