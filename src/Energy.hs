{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE TypeFamilies #-}
module Energy where

import Lattice
import Linear.Metric (Metric, dot, quadrance, normalize)
import Data.Vector.Unboxed
import Data.Vector.Unboxed.Mutable
import Linear.V3 (V3 (V3), triple)
import Linear.Vector ((^-^))
import Types

{-# INLINE uniformExchange #-}
{-# SPECIALISE uniformExchange :: PrimMonad m => Double -> (Index, S2)
                 -> Simulation S2 Double p b m Double #-}
uniformExchange
  :: (PrimMonad m
     ,Metric target
     ,Num base
     ,target base ~ field
     ,Unbox base
     ,Unbox field)
  => base -> (Index, field) -> Simulation field base p b m base
uniformExchange exchJ (i, s) =
  let dd u v = return $ dot u v
  in (* exchJ) <$> baseTargetNeighborContraction dd i s

{-# INLINE interIntraExchange #-}
{-# SPECIALISE interIntraExchange :: PrimMonad m => Double -> Double ->
                 (Index, S2) -> Simulation S2 Double p b m Double #-}
interIntraExchange
  :: forall m target base field latType p b.
     (PrimMonad m
     ,Metric target
     ,Num base
     ,target base ~ field
     ,Unbox base
     ,latType ~ Lattice (PrimState m) field
     ,Unbox field)
  => base -> base -> (Index, field) -> Simulation field base p b m base
interIntraExchange interJ intraJ (k, s) = do
  (Lattice lat) <-
    use (lattice . dataVector) :: Simulation field base p b m latType
  sizes <- use (lattice . dimensions)
  baseNeighborContraction
    (\i j -> do
       let (V3 _ _ z)  = ((toV3 $ hTE sizes j) ^-^ (toV3 $ hTE sizes i)) 
       mj <- unsafeRead lat j
       return $
         if  (z==0)
           then intraJ * (s `dot` mj)
           else interJ * (s `dot` mj))
    k
  where
    hTE = hypercubeTorusEmbedding
    toV3 v =
      V3 (fromIntegral $ v ! 0) (fromIntegral $ v ! 1) (fromIntegral $ v ! 2) :: S2

{-# INLINE antisymmetricExchangeNN #-}
antisymmetricExchangeNN
  :: forall m latType p b.
     (PrimMonad m, latType ~ Lattice (PrimState m) S2)
  => (S2 -> S2)
  -> (Index, S2)
  -> Simulation S2 Double p b m Double
antisymmetricExchangeNN dmi (k, s) = do
  (Lattice lat) <-
    use (lattice . dataVector) :: Simulation S2 Double p b m latType
  sizes <- use (lattice . dimensions)
  baseNeighborContraction
    (\i j -> do
       let rij = normalize $ (toV3 $ hTE sizes j) ^-^ (toV3 $ hTE sizes i)
           {-rij' =
             if quadrance rij >= 2
               then negate (normalize rij)
               else rij -} -- idk wtf this was about
       mj <- unsafeRead lat j
       return $ triple (dmi rij) s mj)
    k
  where
    hTE = hypercubeTorusEmbedding
    toV3 v = V3 (fromIntegral $ v ! 0) (fromIntegral $ v ! 1) 0.0 :: S2

{-# INLINE uniaxialAnisotropy #-}
uniaxialAnisotropy
  :: (PrimMonad m)
  => S2 -> (Index, S2) -> Simulation S2 Double p b m Double
uniaxialAnisotropy k (_, s) = return $ negate $ s `dot` k

{-# INLINE uniaxialAnisotropyZ #-}
uniaxialAnisotropyZ
  :: (PrimMonad m)
  => Double -> (Index, S2) -> Simulation S2 Double p b m Double
uniaxialAnisotropyZ k (_, V3 _ _ z) = return $ negate $ k * z * z

{-# INLINE appliedField #-}
{-# SPECIALISE appliedField ::
                 PrimMonad m =>
                 S2 ->
                   (Index, S2) -> Simulation S2 Double p b m Double
               #-}
appliedField
  :: (PrimMonad m, Metric target, target base ~ field, Num base)
  => field -> (Index, field) -> Simulation field base p b m base
appliedField b (_, s) = return $ negate $ b `dot` s
