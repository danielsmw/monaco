module Options
  ( execParser
  , optionHandler
  , ProgOpts(..)
  ) where 

import Options.Applicative
import Data.Semigroup ((<>))

data ProgOpts = ProgOpts
  { iterationsPerMC :: Int
  , outputDirectory :: FilePath
  , initialTemp :: Double
  , finalTemp :: Double
  , deltaTemp :: Double
  , maxB :: Double
  , deltaB :: Double
  , exchange :: Double
  , dmi :: Double
  , eaa :: Double
  , width :: Int
  , height :: Int
  }

optionHandler :: ParserInfo ProgOpts
optionHandler =
  info
    (programOptions <**> helper)
    (fullDesc <>
     progDesc "Run an adaptive Monte Carlo simulation (annealing and hysteresis)" <>
     header "monaco - a flexible Monte Carlo framework")

programOptions :: Parser ProgOpts
programOptions =
  ProgOpts <$>
  option auto
    ( long "iterations"
    <> short 'n'
    <> help "iterations per MC phase"
    <> value 10000
    <> showDefault
    <> metavar "INT")
  <*> strOption
    ( long "directory"
    <> short 'd'
    <> help "output directory for observer data"
    <> metavar "DIR" )
  <*> option auto
    ( long "initialTemperature"
    <> help "initial temperature for the annealing process"
    <> value (10.0)
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "finalTemperature"
    <> help "final temperature for the annealing process"
    <> value (5.0)
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "deltaTemperature"
    <> help "negative temperature step size for the annealing process"
    <> value (0.5)
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "maxField"
    <> help "magnetic field maximum magnitude for hysteresis"
    <> value (10.0)
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "deltaField"
    <> help "magnetic field step size for hysteresis"
    <> value (10.0)
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "exchange"
    <> short 'J'
    <> help "exchange parameter (positive == AFM)"
    <> value (-5.0)
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "dmi"
    <> short 'D'
    <> help "bulk DMI parameter"
    <> value (0.15)
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "eaa"
    <> short 'K'
    <> help "easy axis anistropy (positive == easy axis)"
    <> value 4.0
    <> showDefault
    <> metavar "DOUBLE")
  <*> option auto
    ( long "width"
    <> short 'w'
    <> help "width of torus"
    <> value 30
    <> showDefault
    <> metavar "INT")
  <*> option auto
    ( long "height"
    <> short 'h'
    <> help "height of torus"
    <> value 30
    <> showDefault
    <> metavar "INT")
