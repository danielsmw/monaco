{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE Rank2Types, TypeFamilies, TemplateHaskell,
  ScopedTypeVariables #-}
module Lattice
  ( baseTargetNeighborContraction
  , baseNeighborContraction
  , hypercubeTorusEmbedding
  , cubicTorus
  , hypertorus
  ) where
import Control.DeepSeq
import Data.Function (on)
import Data.Vector.Unboxed as V
import Data.Vector.Unboxed.Mutable
import Lens.Micro.Platform
import Linear.Metric
import Linear.V3 (V3 ())
import Prelude hiding (read)
import System.Random.MWC
import qualified Data.Vector as BV
import Types

cubicTorus
  :: Vector Int -> Index -> Vector Index
cubicTorus !sizes !i =
  let v = hypercubeTorusEmbedding sizes i
  in fromList $
     (hypercubeTorusCoembedding sizes .
      V.zipWith (flip mod) sizes . V.zipWith (+) v) <$>
     deltas
  where
    deltasP =
      [ [ boole $ k == j
        | k <- [1 .. V.length sizes] ]
      | j <- [1 .. V.length sizes] ]
    deltasList = deltasP Prelude.++ Prelude.map (Prelude.map negate) deltasP
    deltas :: [Vector Int]
    deltas = fromList <$> deltasList
    boole True = 1
    boole False = 0
  
honeycombTorus :: Vector Int -> Index -> Vector Index
honeycombTorus !sizes !i =
  let v = hypercubeTorusEmbedding sizes i
  in fromList $
     (hypercubeTorusCoembedding sizes .
      V.zipWith (flip mod) sizes . V.zipWith (+) v) <$>
     deltas
  where
    deltasP =
      [ [ boole $ k == j
        | k <- [1 .. V.length sizes] ]
      | j <- [1 .. V.length sizes] ]
    deltasList = deltasP Prelude.++ Prelude.map (Prelude.map negate) deltasP
    deltas :: [Vector Int]
    deltas = fromList <$> deltasList
    boole True = 1
    boole False = 0

  
  
{-|
== Common lattice topologies
-}
hypertorus
  :: forall target field m deg k st dim.
     (field ~ target k
     ,Unbox field
     ,Metric target
     ,Num k
     ,PrimMonad m
     ,st ~ PrimState m)
  => Gen st
  ->(Index -> Vector Index)
  -> Initialization m field
  -> Vector Int
  -> m (LatticeData st field)
hypertorus g neighbors initz sizes = do
  latticeVector <- (new $ V.product sizes)
  sequence_
    [ initValue j >>= write latticeVector j
    | j <- [0 .. pred (V.product sizes)] ]
  return $!!
    LatticeData
    { _dataVector = Lattice latticeVector
    , _dimensions = sizes
    , _graphTopology = neighborVec
    , _totalNodes = V.product sizes
    }
  where
    initValue j =
      case initz of
        Constant field -> return field
        Functional f -> return $ f j
        Randomized f -> f g j
    neighborVec =
      BV.fromList $
      [ neighbors j
      | j <- [0 .. pred (V.product sizes)] ]

hypercubeTorusCoembedding :: Vector Int -> Vector Int -> Index
hypercubeTorusCoembedding sizes xs = V.sum $ V.zipWith (*) xs layers
  where
    layers = 1 `cons` (V.scanl1 (*) sizes)
  
{-
hypercubeTorusEmbedding
  :: Vector Int -> Index -> Vector Int
hypercubeTorusEmbedding sizes j =
  hypercubeTorusEmbedding' layers j (pred $ V.length sizes)
  where
    layers = V.scanl1 (*) sizes
     
hypercubeTorusEmbedding' :: Vector Int -> Index -> Int -> Vector Int
hypercubeTorusEmbedding' layers j (-1) = empty
hypercubeTorusEmbedding' layers j acc =
  (j `quot` (layers ! acc)) `cons`
  hypercubeTorusEmbedding' layers (j `rem` (layers ! acc)) (pred acc)
-}

{-

The index at (i,j,k,...l) for dimensions (w,x,y,...z) is

  ix = i + w j + w x k + ... + (w x ... y) l

So that

  i = ix `mod` w
  j = ix `mod` (w x) - i
  k = ix `mod` (w x y) - i - w j

and so on.

Let the dimensions product array be

dProds = scanl1 (*) dims

The correct prototype function is

let ixss j = Prelude.zipWith (quot) (Prelude.zipWith (-) (Prelude.map
(rem j) dProds) $ Prelude.zipWith (*) (0:1:dProds) (0:ixss j))
(1:dProds)

-}
  
{-# INLINE hypercubeTorusEmbedding #-}
hypercubeTorusEmbedding :: Vector Int -> Index -> Vector Index
hypercubeTorusEmbedding ds j = fromList (hypercubeTorusEmbedding' ds j)
hypercubeTorusEmbedding' :: Vector Int -> Index -> [Int]
hypercubeTorusEmbedding' ds j =
  Prelude.zipWith
    quot
    (Prelude.zipWith (-) (Prelude.map (rem j) dProds) $
     Prelude.zipWith (*) (0 : 1 : dProds) (0 : hypercubeTorusEmbedding' ds j))
    (1 : dProds)
  where
    dProds = toList $ V.scanl1' (*) ds

{-
  V.map fst $
  V.scanl f (j `rem` dProd 1, j) $
  V.fromList
    [ (ds ! i, dProd i)
    | i <- [1 .. pred (V.length ds)] ]
  where
    dProd n = V.product $ V.take n ds
    f (x, i) (d, dd) = let i' = (i - x) `quot` d
                        in (i' `rem` dd, i')-}
  
hypercubeTorusNeighbors :: [(Int, Int)]
                        -> Vector Int
                        -> Index
                        -> [Index]
hypercubeTorusNeighbors [] stepSizes j = []
hypercubeTorusNeighbors ((d, size):sizes) stepSizes j =
  let predD'
        | j `rem` size == 0 = j + (stepSizes ! d) * (pred size)
        | otherwise = j - (stepSizes ! d)
      succD'
        | j `rem` size == (size - 1) = j - (stepSizes ! d) * (pred size)
        | otherwise = j + (stepSizes ! d)
      succD =
        case (cmp succD' j) of
          EQ -> succD'
          LT -> succD' + (stepSizes ! (d + 1))
          GT -> succD' - (stepSizes ! (d + 1))
      predD =
        case (cmp predD' j) of
          EQ -> predD'
          LT -> predD' + (stepSizes ! (d + 1))
          GT -> predD' - (stepSizes ! (d + 1))
  in predD : succD : hypercubeTorusNeighbors sizes stepSizes j
  where
    cmp = compare `on` (`quot` (stepSizes ! (d + 1)))
{-
hypercubeTorusValidation :: [Int] -> [(Int,[Int],[Int])]
hypercubeTorusValidation sizes =
  let hc = hypercubeTorus sizes
      nn = hc ^. neighborMap
  in Prelude.filter
       (\(j, a, b) -> sort a /= sort b)
       [ (j, (nn j), (explicitNN j))
       | j <- [0 .. pred (Prelude.product sizes)] ]
  where
    deltasP =
      [ [ boole $ i == j
        | i <- [1 .. Prelude.length sizes] ]
      | j <- [1 .. Prelude.length sizes] ]
    deltas = deltasP Prelude.++ Prelude.map (Prelude.map negate) deltasP
    boole True = 1
    boole False = 0
    ss = fromList sizes
    explicitNN j =
      let pt = hypercubeTorusEmbedding ss j
          ds = (Prelude.zipWith (+) pt) <$> deltas
      in hypercubeTorusCoembedding ss <$>
         Prelude.map (Prelude.zipWith (flip b mod) sizes) ds 
-}   
  
{-|
== Summing over local interactions

The following functions are useful to representing nearest neighbor
interactions.
-}
  
{- SPECIALISE targetNeighborContraction :: Lattice RealWorld (V3
                 Double) -> (V3 Double -> V3 Double ->
                 Double) -> Index -> ReaderT Parameters IO Double #-}
{-| For the given lattice, apply the given two argument function to the
  the value at given lattice index as well as all of its neighbors,
  and sum the result. In other words (in a shortened pseudocode)

  >>> targetNeighborContraction m f i
  return $ sum [ f (m i) (m (i+1)) , f (m i) (m (i-1)) , ... ]

  The set of neighbors is generated by the 'neighborMap' property of the
  'LatticeData' held in the Reader monad. By construction, this function
  limits the access of the given map to the target space of the fields.
  If you need information about the local geometry during contraction,
  consider 'baseNeighborContraction'.
-}
  {-
targetNeighborContraction
  :: forall target k m field latType.
     (Unbox (target k)
     ,Unbox k
     ,latType ~ Lattice (PrimState m) field
     ,field ~ target k
     ,PrimMonad m
     ,Num k)
  => (target k -> target k -> Simulation m field k k)
  -> Index
  -> Simulation m field k k
targetNeighborContraction f j = do
  neighbors <- view $ lattice . graphTopology . ix j
  (Lattice lat) <- view (lattice . dataVector) :: Simulation m field k latType
  v <- lat `read` j
  neighborVals <- lift $ V.mapM (read lat) neighbors
  V.sum <$> ((f v) `V.mapM` neighborVals)
-}
{-# INLINE baseNeighborContraction #-}
{-# SPECIALISE baseNeighborContraction ::
                 PrimMonad m =>
                 (Index -> Index -> Simulation (V3 Double) Double p b m Double) ->
                   Index -> Simulation (V3 Double) Double p b m Double
               #-}
baseNeighborContraction
  :: (Unbox k
     ,PrimMonad m
     ,Num k)
  => (Index -> Index -> Simulation field k p b m k)
  -> Index
  -> Simulation field k p b m k
baseNeighborContraction f j = do
  neighbors <- use (lattice.graphTopology.ix j)
  V.sum <$> V.mapM (f j) neighbors

{-# INLINE baseTargetNeighborContraction #-}
{-# SPECIALISE baseTargetNeighborContraction ::
                 PrimMonad m =>
                 ((V3 Double) ->
                    (V3 Double) -> Simulation (V3 Double) Double p b m Double)
                   -> Index -> (V3 Double) -> Simulation (V3 Double) Double p b m Double
               #-}
baseTargetNeighborContraction
  :: forall target k m field p latType b.
     (Unbox (target k)
     ,Unbox k
     ,field ~ target k
     ,PrimMonad m
     ,Num k)
  => (field -> field -> Simulation field k p b m k)
  -> Index
  -> field
  -> Simulation field k p b m k
baseTargetNeighborContraction !f !j !v = do
  neighbors <- use $ lattice . graphTopology . ix j
  (Lattice lat) <- use (lattice . dataVector) 
  neighborVals <- V.mapM (unsafeRead lat) neighbors
  V.sum <$> ((f v) `V.mapM` neighborVals)
