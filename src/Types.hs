{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
module Types
  ( Lattice (..)
  , latticeMV
  , Parameters (..)
  , lattice
  , rng
  , metropolis
  , observations
  , thermalObservations
  , MetropolisParams (..)
  , interactions
  , acceptanceCriteria
  , perturbation
  , temperature
  , maximumIterations
  , thermalSampling
  , LatticeData (..)
  , dimensions
  , totalNodes
  , graphTopology
  , dataVector
  , Simulation
  , AcceptanceCriteria
  , Interaction (..)
  , UnboxedInteraction
  , unboxInteraction
  , Perturbation (..)
  , UnboxedPerturbation
  , Index
  , Initialization (..)
  , PrimMonad
  , PrimState
  , Gen
  , Temperature(..)
  , Energy(..)
  , S2
  , MaximumTangent
  , V3 (..)
  , observer, collectedData, Observation(..), ObservationDispatch (..)
  , observerID, unboxObserverID, active, ObserverID(..)
  , InteractionParameter (..)
  , (<!)
  , module Lens.Micro.Platform
) where

import Control.DeepSeq
import Control.Monad.Primitive (PrimMonad, PrimState)
import Control.Monad.State.Strict
import Data.Map
import Data.Ratio
import Data.Complex (Complex (..))
import Data.Vector.Unboxed as V
import Linear.V1 (V1(..))
import Linear.V3 (V3(..))
import Linear.Vector
import GHC.Generics
import Lens.Micro.Platform 
import System.Random.MWC
import qualified Data.Vector as BV
import Codec.Picture (PixelRGB8, Image)

newtype Temperature =
  Temperature Double
  deriving (Ord, Eq, Show)

instance Num Temperature where
  (+) (Temperature a) (Temperature b) = Temperature (a + b)
  (*) (Temperature a) (Temperature b) = Temperature (a * b)
  negate (Temperature a) = Temperature $ negate a
  fromInteger x = Temperature (fromIntegral x)
  abs (Temperature x) = Temperature (abs x)
  signum (Temperature x) = Temperature (signum x)

newtype Energy = Energy Double
type S2 = V3 Double
type MaximumTangent = Double

data InteractionParameter field k p b
  = BaseParameter k
  | FieldParameter field
  | SweepParameter b
  | RealParameter {-# UNPACK #-} Double
  | ComplexParameter (Complex Double)
  | PairParameter (InteractionParameter field k p b)
                  (InteractionParameter field k p b)
  deriving (Generic, NFData)

instance (Show field, Show b, Show k) =>
         Show (InteractionParameter field k p b) where
  show (BaseParameter k) = show k
  show (FieldParameter field) = show field
  show (SweepParameter b) = show b
  show (RealParameter r) = show r
  show (ComplexParameter c) = show c
  show (PairParameter p1 p2) = show $ (p1, p2)
  
newtype Lattice st field = Lattice
  { _latticeMV :: (MVector st field)
  } deriving (Generic, NFData)

type Index = Int

type Simulation field k p b m = StateT (Parameters field k p b m) m

type AcceptanceCriteria field k p b m = Gen (PrimState m) -> k -> Simulation field k p b m Bool

data Perturbation field k p b m
  = ParameterizedPerturbation (p -> UnboxedPerturbation field k p b m)
  | Perturbation (UnboxedPerturbation field k p b m)
  deriving (Generic, NFData)

type UnboxedPerturbation field k p b m = field -> Simulation field k p b m field

type UnboxedInteraction field k p b m = (Index, field) -> Simulation field k p b m k

data Interaction field k p b m
  = Interaction { name :: String
                , param :: InteractionParameter field k p b
                , operator :: InteractionParameter field k p b -> UnboxedInteraction field k p b m}
  | TemperatureInteraction { interactionTemperature :: Double}
  deriving (Generic, NFData)

instance Eq (Interaction field k p b m) where
  (==) a b = name a == name b

instance Show (Interaction field k p b m) where
  show = name
  
unboxInteraction :: Interaction field k p b m -> UnboxedInteraction field k p b m
unboxInteraction i = operator i $ param i

data MetropolisParams field k p b m = MetropolisParams
  { _interactions :: Map String (Interaction field k p b m)
  , _acceptanceCriteria :: AcceptanceCriteria field k p b m
  , _perturbation :: Perturbation field k p b m
  , _temperature :: {-# UNPACK #-} Double
  , _maximumIterations :: {-# UNPACK #-} Int
  , _thermalSampling :: Maybe (Ratio Int)
  } deriving (Generic, NFData)

(<!) :: Ord k => Map k a -> k -> Maybe a 
(<!) m k = Data.Map.lookup k m

data Parameters field base p b m = Parameters
  { _lattice :: LatticeData (PrimState m) field
  , _rng :: Gen (PrimState m)
  , _metropolis :: MetropolisParams field base p b m
  , _observations :: Map ObserverID (ObservationDispatch field base p b m)
  , _thermalObservations :: Map ObserverID (ObservationDispatch field base p b m)
  }

data Observation field k p b m
  = ScalarObservation (Maybe (Interaction field k p b m), Double)
  | ImageObservation (Maybe (Interaction field k p b m), Image PixelRGB8)
  deriving (Generic, NFData)

instance Show (Observation field k p b m) where
  show (ScalarObservation (_, d)) = show d
  show (ImageObservation (_, _)) = "<image-data>"
  
unboxObserverID :: ObserverID -> String
unboxObserverID (ObserverID s) = s

newtype ObserverID = ObserverID String deriving (Eq, Ord, Show, Generic, NFData)

data ObservationDispatch field k p b m = ObservationDispatch
  { _observer :: (Simulation field k p b m (Observation field k p b m))
  , _collectedData :: BV.Vector (Observation field k p b m)
  , _observerID :: ObserverID
  , _active :: Bool
  } 

data LatticeData st field = LatticeData
  { _dataVector :: Lattice st field
  , _dimensions :: Vector Index
  , _totalNodes :: {-# UNPACK #-} Int
  , _graphTopology :: BV.Vector (Vector Index) } deriving (Generic, NFData)

makeLenses ''MetropolisParams
makeLenses ''Parameters
makeLenses ''LatticeData
makeLenses ''Lattice
makeLenses ''ObservationDispatch

data Initialization m field
  = Constant field
  | Randomized (Gen (PrimState m) -> Index -> m field)
  | Functional (Index -> field)
