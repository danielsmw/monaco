{-# LANGUAGE DeriveFunctor #-}
module Interpreter
  ( anneal
  , hysteresis
  , registerObserver
  , unregisterObserver
  , activateObserver
  , deactivateObserver
  , interpret
  , ObserverType (..)
  , Experiment ()
  , Iterations (..)
  , LowHighDelta (..)
  ) where

import Types
import Energy (appliedField)
import MonteCarlo (metropolisSequence)
import Simulation (annealSweep, interactionScan)

import Control.Monad.Free
import Control.Monad.Primitive

import Data.Map (delete, insert, fromList, adjust)
import qualified Data.Vector as BV
import Linear.Vector ((*^))
import Linear.Metric (normalize)


{-| The 'Experiment' data type represents a small AST for a mini-language
    out of which we can compose common simulation steps with each
    other. An 'Experiment' supports smoothly changing the temperature
    up or down via 'anneal', changing the applied field via
    'hysteresis', and the installing and managing of
    'ObserverDispatch' types which record data concerning the
    simulation state at various appropriate times. -}

data Experiment m next
  = Anneal Iterations
           (LowHighDelta Temperature)
           next
  | Hysteresis Iterations
               S2
               (LowHighDelta Double)
               next
  | RegisterObserver ObserverType
                     (ObservationDispatch S2 Double Double S2 m)
                     next
  | UnregisterObserver ObserverType
                       ObserverID
                       next
  | ActivateObserver ObserverType
                     ObserverID
                     next
  | DeactivateObserver ObserverType
                       ObserverID
                       next
  | Message String
            next
  | Trivial next
  deriving (Functor)

newtype Iterations = Iterations Int
newtype LowHighDelta a = LowHighDelta (a,a,a) deriving Show

{-| An 'ObservationDispatch' can either take a 'Thermal' ensemble of
    measurements throughout the course of a 'metropolisSequence', or
    it can simply 'Sample' the final state after the sequence is
    complete. -}
data ObserverType = Thermal | Sample
  
anneal :: Iterations -> LowHighDelta Temperature -> Free (Experiment m) ()
anneal i kts = liftF (Anneal i kts ())

hysteresis :: Iterations -> S2 -> LowHighDelta Double -> Free (Experiment m) ()
hysteresis i m bs = liftF (Hysteresis i m bs ())

registerObserver
  :: ObserverType
  -> ObservationDispatch S2 Double Double S2 m
  -> Free (Experiment m) ()
registerObserver otype obs = liftF (RegisterObserver otype obs ())

unregisterObserver :: ObserverType -> ObserverID -> Free (Experiment m) ()
unregisterObserver otype obs = liftF (UnregisterObserver otype obs ())

deactivateObserver :: ObserverType -> ObserverID -> Free (Experiment m) ()
deactivateObserver otype obs = liftF (DeactivateObserver otype obs ())

activateObserver :: ObserverType -> ObserverID -> Free (Experiment m) ()
activateObserver otype obs = liftF (ActivateObserver otype obs ())

{-| Interpret an 'Experiment' specification, yielding the actual
    'Simulation' to be run. -}
interpret
  :: (PrimMonad m)
  => Free (Experiment m) r -> Simulation S2 Double Double S2 m ()
interpret cmd =
  case cmd of
    Free (Anneal (Iterations i) kts x) ->
      annealSweep (asList kts) (metropolisSequence i) >> interpret x
    Free (Hysteresis (Iterations i) m bs x) ->
      interactionScan
        (BV.fromList (asList bs))
        (\b ->
            Interaction
            { name = "B_z"
            , param = FieldParameter (b *^ normalize m)
            , operator = \(FieldParameter s) -> appliedField s
            })
        (metropolisSequence i) >>
      interpret x
    Free (RegisterObserver otype obs x) -> do
      ((obsSelect otype) %= (insert (obs ^. observerID) obs)) >> interpret x
    Free (UnregisterObserver otype obsID x) -> do
      ((obsSelect otype) %= delete obsID) >> interpret x
    Free (DeactivateObserver otype obsID x) -> do
      ((obsSelect otype) %= adjust setUnactive obsID) >> interpret x
    Free (ActivateObserver otype obsID x) -> do
      ((obsSelect otype) %= adjust setActive obsID) >> interpret x
    Pure _ -> return ()
  where
    setUnactive od =
      od
      { _active = False
      }
    setActive od =
      od
      { _active = True
      }
    obsSelect Thermal = thermalObservations
    obsSelect Sample = observations

asList :: (Ord a, Num a, Show a) => LowHighDelta a -> [a]
asList (LowHighDelta (i, f, d)) =
  if (signum (f - i) == signum d)
    then i : asList (LowHighDelta (i + d, f, d))
    else [] 
