{-# LANGUAGE Strict #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
module Observations
  ( onRegion
  , scalarOnRegion
  , magnetizationOnRegion
  , layerMap3D
  , imageObserver
  , imageChargeObserver
  , halfHslFromS2_RGB8
  , hslFromS2_RGB8
  , hslFromS2_RGB8_BW
  , Observation(..)
  , magnetizationObserver
  , skyrmionObserver
  , runObservations
  , runThermalObservations
  , datasetHandler
  --, thermalDatasetHandler -- don't expose this function, since it's currently broken.
  , IndependentVariable(..)
  ) where

import Types

import Codec.Picture
       (Image, PixelRGB8, generateImage, PixelRGB8(..), writePng)
import Control.Monad.State (get, filterM, liftM)
import Control.DeepSeq

import Data.Complex
import Data.Map (Map, elems, intersectionWith)
import Data.Colour.RGBSpace (uncurryRGB)
import Data.Colour.RGBSpace.HSL (hsl)
import Data.Colour.RGBSpace.HSV (hsv)
import Data.Functor.Identity as I (runIdentity, Identity(Identity))

import qualified Data.Vector as BV
       (sequence, sum, cons, snoc, zipWith, Vector(..), head, takeWhile,
        drop, length)
import Data.Vector.Unboxed as U (Unbox, (!), freeze) 
import Data.Vector.Unboxed.Mutable (unsafeRead) 
import Data.Traversable as T (sequence)
import Data.Vector.Storable as S
       (generateM, Storable, convert, freeze, (!))

import Linear.Metric
import Linear.V3
import Linear.Vector
import System.IO
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector as BV
       (filter, map, mapM, mapM_, fromList, empty)

import Debug.Trace

-- This is the data type in which we record series of observations
type Dataset field k p b m = BV.Vector (Observation field k p b m)


{-| 'thermalDatasetHandler' is supposed to act like 'datasetHandler',
  except that it averages over observations taken at the same value of
  a given independent variable. Unfortunately, it doesn't work, and I
  didn't have time to track down the bug at the time. So in practice
  we just use 'datasetHandler', printing out /all/ the data, and then
  do the averages in post-processing using the perl script averager.pl. -}
thermalDatasetHandler
  :: (Show b, Show k, Show field)
  => FilePath -> Dataset field k p b m -> IO ()
thermalDatasetHandler fp xs' =
  case (BV.head xs) of
    (ScalarObservation _) -> do
      putStrLn ("writing to " ++ fp ++ ".tsv ...")
      fh <- openFile (fp ++ ".tsv") WriteMode
      BV.mapM_ (hPutStrLn fh) $
        (\(ScalarObservation (int, y)) ->
            (maybe "Nothing" showParam int) ++ "\t" ++ (show y)) <$>
        xs
      hClose fh
    (ImageObservation _) ->
      trace "Thermal image observation not implemented." $ return ()
  where
    showParam (Interaction _ p _) = show p
    showParam (TemperatureInteraction t) = show t
    xs = thermalAverage xs'

{-| The function that should have, but doesn't, correctly perform
  thermal averages. -}
thermalAverage :: BV.Vector (Observation field k p b m)
               -> BV.Vector (Observation field k p b m)
thermalAverage v =
  if (BV.length v == 0)
    then BV.empty
    else let ensemble = BV.takeWhile ((==) (domVal $ BV.head v) . domVal) v
         in ScalarObservation ((domVal $ BV.head v), avg ensemble) `BV.cons`
            (thermalAverage $ BV.drop (BV.length ensemble) v)
  where
    domVal (ScalarObservation (m, _)) = m
    domVal (ImageObservation (m, _)) = m
    avg u = (BV.sum $ asValue <$> u) / (fromIntegral $ BV.length u)
    asValue (ScalarObservation (_, d)) = d
    asValue (ImageObservation (_, _)) =
      error "Thermal image observation not implemented."
  
{-| For the given directory, and file name prefix, write a dataset to
    disk. Datasets containing scalar data are written as tab-separated
    value files, just using the default Show instances for the various
    types. The images, meanwhile, are written as PNGs using the
    JuicyPixels library (Codec.Picture). -}
datasetHandler 
  :: (Show b, Show k, Show field)
  => FilePath -> FilePath -> Dataset field k p b m -> IO ()
datasetHandler dir fp xs =
  if (BV.length xs == 0)
    then putStrLn $ "warning: no data recorded for " ++ show fp
    else case (BV.head xs) of
           (ScalarObservation _) -> do
             fh <- openFile (dir ++ "/" ++ fp ++ ".tsv") WriteMode
             BV.mapM_ (hPutStrLn fh) $
               (\(ScalarObservation (int, y)) ->
                   (maybe "Nothing" showParam int) ++ "\t" ++ (show y)) <$>
               xs
             hClose fh
           (ImageObservation _) ->
             BV.mapM_ (uncurry writePng) $
             (\(ImageObservation (int, y)) ->
                 ( dir ++
                   "/" ++ fp ++ "_" ++ (maybe "Nothing" showParam int) ++ ".png"
                 , y)) <$>
             xs
  where
    showParam (Interaction _ p _) = show p
    showParam (TemperatureInteraction t) = show t
                                              
{-
observationHandler
  :: Show b
  => (b, Observation m field base p) -> IO ()
observationHandler (b, ScalarObservation (_, x)) =
  putStrLn $ show b ++ "\t" ++ show x
observationHandler (b, ImageObservation (_, img)) =
  writePng ("image-b" ++ show b ++ ".png") img
-}

-- An independent variable (an "x-axis" of the output data) is either an interaction parameter or the temperature.
data IndependentVariable
  = InteractionIV String
  | TemperatureIV

{-# SPECIALISE onRegion :: PrimMonad m => (Index -> Bool) -> (V3 Double
                 -> I.Identity Double) -> Simulation (V3 Double)
                 Double Double S2 m (I.Identity Double) #-}
onRegion
  :: forall target field base f a latType m p b.
     (Metric target
     ,field ~ target base
     ,Unbox field
     ,Num base
     ,Unbox base
     ,Num a
     ,Additive f
     ,Applicative f
     ,latType ~ Lattice (PrimState m) field
     ,PrimMonad m)
  => (Index -> Bool)
  -> (field -> f a)
  -> Simulation field base p b m (f a)
onRegion reg f = do
  (Lattice lat) <- use (lattice . dataVector) :: Simulation field base p b m latType
  n <- use (lattice . totalNodes)
  domIx <- return $ BV.filter reg $ BV.fromList [0 .. pred n]
  domField <- unsafeRead lat `BV.mapM` domIx
  return . sumV $ BV.map f domField

{-# SPECIALISE scalarOnRegion :: PrimMonad m => (Index -> Bool) -> (V3 Double
                 -> Double) -> Simulation (V3 Double)
                 Double Double S2 m (Double) #-}
scalarOnRegion
  :: (Metric target
     ,field ~ target base
     ,Unbox field
     ,Num base
     ,Unbox base
     ,Num a
     ,latType ~ Lattice (PrimState m) field
     ,PrimMonad m)
  => (Index -> Bool)
  -> (field -> a)
  -> Simulation field base p b m a
scalarOnRegion reg f = runIdentity <$> onRegion reg (I.Identity . f)
  -- the nonsense with wrapping things up in the identity functor has
  -- to do with some high minded idealism I once had about running
  -- everything through 'onRegion'. Turns out I never ran /anything/
  -- through onRegion, so this is a bit of cruft that should really be
  -- refactored, but, whatever. I doubt it adds much overhead.
  
magnetizationOnRegion
  :: forall m p.
     PrimMonad m
  => (Index -> Bool)
  -> IndependentVariable
  -> S2
  -> Simulation S2 Double p S2 m (Observation S2 Double p S2 m)
magnetizationOnRegion reg (InteractionIV intName) s = do
  ints <- use (metropolis . interactions) 
  scalarOnRegion reg (dot s) >>=
    (return . ScalarObservation . (,) (ints <! intName))
magnetizationOnRegion reg TemperatureIV s = do
  t <- use (metropolis . temperature) 
  scalarOnRegion reg (dot s) >>=
    (return . ScalarObservation . (,) (Just $ TemperatureInteraction t))

magnetizationObserver
  :: PrimMonad m
  => (Index -> Bool)
  -> IndependentVariable
  -> S2
  -> ObserverID
  -> ObservationDispatch S2 Double p S2 m 
magnetizationObserver reg intName s obsID =
  ObservationDispatch {
    _collectedData = BV.empty,
    _observer = (magnetizationOnRegion reg intName s) ,
    _observerID = obsID,
    _active = True }

{-# SPECIALIZE skyrmionObserver :: PrimMonad m => IndependentVariable -> ObserverID -> ObservationDispatch S2 Double Double S2 m #-}
skyrmionObserver
  :: PrimMonad m
  => IndependentVariable -> ObserverID -> ObservationDispatch S2 Double p S2 m
skyrmionObserver intName obsID =
  ObservationDispatch {
    _collectedData = BV.empty,
    _observer = skyrmionNumber intName,
    _observerID = obsID,
    _active = True }

{-# SPECIALIZE skyrmionNumber :: PrimMonad m => IndependentVariable -> Simulation S2 Double Double S2 m (Observation S2 Double Double S2 m) #-}
skyrmionNumber
  :: forall m p.
     PrimMonad m
  => IndependentVariable -> Simulation S2 Double p S2 m (Observation S2 Double p S2 m)
skyrmionNumber (InteractionIV intName) = do
  ints <- use (metropolis . interactions)
  lat <- use (lattice . dataVector)
  n <- use (lattice . totalNodes)
  (sum <$> mapM (skyrmionNumber' lat) [0 .. pred n]) >>=
    (return . ScalarObservation . (,) (ints <! intName))
skyrmionNumber TemperatureIV = do
  t <- use (metropolis . temperature)
  lat <- use (lattice . dataVector)
  n <- use (lattice . totalNodes)
  (sum <$> mapM (skyrmionNumber' lat) [0 .. pred n]) >>=
    (return . ScalarObservation . (,) (Just $ TemperatureInteraction t))

{-# SPECIALIZE skyrmionNumber' :: PrimMonad m => Lattice (PrimState m) (V3 Double) -> Int -> Simulation (V3 Double) Double Double S2 m Double #-}
skyrmionNumber'
  :: forall target field base m p b.
     (target ~ V3
     ,Metric target
     ,field ~ target base
     ,Unbox (target base)
     , Show base
     ,Num base
     ,RealFloat base
     ,Unbox base
     ,PrimMonad m)
  => Lattice (PrimState m) field -> Int -> Simulation field base p b m base
-- What I'm going to do here is extremely dangerous, because I'm
-- relying on a specific ordering returned by cubicTorus in choosing
-- how the products are taken.
skyrmionNumber' (Lattice lat) j = do
  neighbors <- use $ lattice . graphTopology . ix j
  neighborVals <- V.mapM (unsafeRead lat) neighbors
  m <- unsafeRead lat j
  let dx = neighborVals U.! 0
      dy = neighborVals U.! 1
      dx' = neighborVals U.! 2
      dy' = neighborVals U.! 3
      sU = triple m dx dy :+ 0.0
      sD = triple m dx' dy' :+ 0.0
      u0 = m <.> dx
      u1 = dy <.> dx
      u2 = m <.> dy
      u0' = m <.> dx'
      u1' = dy' <.> dx'
      u2' = m <.> dy'
      pU = sqrt $ 2 * (1 + u0) * (1 + u1) * (1 + u2)
      pD = sqrt $ 2 * (1 + u0') * (1 + u1') * (1 + u2')
      wU = phase $ (1 + u1 + u2 + u0 + i * sU) / pU
      wD = phase $ (1 + u1' + u2' + u0' + i * sD) / pD
      phi = 2 * (wU + wD)
  return $ {-traceShow (m, dx, dy, wU, wD)-} phi
  where
    (<.>) a b = (a `Linear.Metric.dot` b) :+ 0.0
    i = 0.0 :+ 1.0


layerMap3D :: [Int] -> Int -> (Int -> Int -> Index)
layerMap3D (xN:yN:zN:[]) z =
  if z >= zN
    then error
           "layerMap3D is requesting layers beyond the available dimensionality of the z-component."
    else \x y -> xN * yN * z + xN * y + x
layerMap3D _ _ =
  error
    "layerMap3D was called with a list of dimensions of length other than 3."

imageObserver
  :: (Unbox field, PrimMonad m)
  => (Int -> Int -> Index)
  -> (field -> PixelRGB8)
  -> IndependentVariable
  -> Int
  -> Int
  -> ObserverID
  -> ObservationDispatch field k p b m
imageObserver ixFunction asColor intName xN yN obsID =
  ObservationDispatch
  { _collectedData = (BV.empty)
  , _observer =
    do ints <- use (metropolis . interactions)
       t <- use (metropolis . temperature)
       (Lattice v) <- use (lattice . dataVector)
       w <- U.freeze v
       return . ImageObservation . (,) (indepVar ints t) $
         generateImage (generator w) xN yN
  , _observerID = obsID
  , _active = True
  }
  where
    generator w x y = asColor $ (U.!) w $ ixFunction x y
    indepVar ints t =
      case intName of
        (InteractionIV i) -> ints <! i
        TemperatureIV -> Just $ TemperatureInteraction t

imageChargeObserver
  :: (Unbox field
     ,field ~ V3 k
     ,PrimMonad m
     ,Show k
     ,RealFloat k
     ,Unbox k
     ,Storable k)
  => (Int -> Int -> Index)
  -> (k -> PixelRGB8)
  -> IndependentVariable
  -> Int
  -> Int
  -> ObserverID
  -> ObservationDispatch field k p b m
imageChargeObserver ixFunction asColor intName xN yN obsID =
  ObservationDispatch
  { _collectedData = (BV.empty)
  , _observer =
    do ints <- use (metropolis . interactions)
       t <- use (metropolis . temperature)
       n <- use (lattice . totalNodes)
       l <- use (lattice . dataVector)
       wQ <- generateM n (skyrmionNumber' l) 
       return . ImageObservation . (,) (indepVar ints t) $
         generateImage (generator wQ) xN yN
  , _observerID = obsID
  , _active = True
  }
  where
    generator w x y = asColor $ (S.!) w $ ixFunction x y
    indepVar ints t =
      case intName of
        (InteractionIV i) -> ints <! i
        TemperatureIV -> Just $ TemperatureInteraction t

hslFromS2_RGB8_BW :: Double -> PixelRGB8
hslFromS2_RGB8_BW z =
  uncurryRGB PixelRGB8 . fmap (\w -> round (w * 255.0)) $
  hsv h ((z + 1.0) / 2.0) ((z + 1.0) / 2.0)
  where h = if z > 0 then 0 else 240.0


hslFromS2_RGB8 :: V3 Double -> PixelRGB8
hslFromS2_RGB8 (V3 x y z) =
  uncurryRGB PixelRGB8 . fmap (\w -> round (w * 255.0)) $
  hsl (phi / (2.0 * pi) * 360.0) 1.0 ((cos theta + 1.0) / 2.0)
  where
    theta = acos z
    phi = atan2 y x

halfHslFromS2_RGB8 :: V3 Double -> PixelRGB8
halfHslFromS2_RGB8 (V3 x y z) =
  uncurryRGB PixelRGB8 . fmap (\w -> round (w * 255.0)) $
  hsl (phi / (2.0 * pi) * 360.0) 1.0 ((cos theta + 1.0) / 2.0)
  where
    theta = if z < 0 then acos z else pi - acos z
    phi = atan2 y x

{-# SPECIALISE runObservations ::
                 PrimMonad m => Simulation (V3 Double) Double Double S2 m () #-}
runObservations
  :: (Show (Observation field k p b m), PrimMonad m)
  => Simulation field k p b m ()
runObservations = do
  newData <- ((maybeObserve <$>) <$> (use $ observations)) >>= T.sequence
  oldData <- (_collectedData <$>) <$> (use $ observations)
  observations %=
    \obs ->
       intersectionWith
         (\obd dat ->
             obd
             { _collectedData = dat 
             })
         obs $
       intersectionWith
         (\old new ->
             case new of
               Nothing -> old
               Just obv -> BV.snoc old obv)
         oldData
         newData
  where
    maybeObserve od =
      if _active od
        then (liftM Just $ _observer od)
        else (return Nothing)


{-# SPECIALISE runThermalObservations ::
                 PrimMonad m => Simulation (V3 Double) Double Double S2 m () #-}
runThermalObservations
  :: (NFData b
     ,NFData field
     ,NFData k
     ,Show (Observation field k p b m)
     ,PrimMonad m)
  => Simulation field k p b m ()
runThermalObservations = do
  newData <- (maybeObserve <$>) <$> (use $ thermalObservations) >>= T.sequence
  oldData <- (_collectedData <$>) <$> (use $ thermalObservations)
  thermalObservations %=
    \obs ->
       intersectionWith
         (\obd dat ->
             obd
             { _collectedData = force dat
             })
         obs $
       intersectionWith
         (\old new ->
             case new of
               Nothing -> old
               Just obv -> BV.snoc old obv)
         oldData
         newData
  where
    maybeObserve od =
      if _active od
        then (liftM Just $ _observer od)
        else (return Nothing)
