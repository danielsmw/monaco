{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TypeFamilies #-}
module Main where

import Control.Monad.Primitive
import Lens.Micro.Platform
import Control.Monad.Reader
import Linear.V3
import Linear.Vector
import Linear.Metric
import Data.Vector.Unboxed as V
import Data.Vector as BV
import Data.Vector.Unboxed.Mutable as MV
import Data.Maybe
import Lattice
import MonteCarlo
import Energy
import System.Random.MWC
import System.Environment
import Criterion.Main (defaultMain)

main :: IO ()
main = do
  n:_ <- getArgs
  let sizes = V.fromList [10,10]
  ps <- withSystemRandom (flip buildParams sizes)
  print =<< V.freeze (ps^.lattice.dataVector.latticeMV)
  print =<< runReaderT ((ps^.metropolis.acceptanceCriteria) 10.0) ps
  print "Okay, simming now."
  v <- runReaderT (simpleSimulation (Prelude.read n)) ps
  print v

buildParams :: GenIO -> V.Vector Int -> IO (Parameters IO RealWorld S2 Double)
buildParams g sizes = do
  ld <-
    hypertorus g (cubicTorus sizes) (Randomized $ \g' _ -> randomSpherical g') sizes
  return $ Parameters ld g metroParams
  where
    metroParams =
      MetropolisParams
      { _interactions = [uniformExchange 1.0]
      , _acceptanceCriteria = boltzmannAccept 10.0
      , _perturbation = walkSpinHector 5.0
      }

type MaximumTangent = Double
type S2 = V3 Double

walkSpinHector :: PrimMonad m => MaximumTangent -> S2 -> Simulation m S2 Double S2
walkSpinHector !m !s = do
  g <- view rng
  u <- randomSpherical g
  w <- uniformR (0.0, m) g
  let ds = w *^ (normalize $ u ^-^ project s u)
  return $ normalize $ s ^+^ ds

simpleSimulation
  :: (PrimMonad m, Num k, field ~ target k, Unbox k, Unbox field)
  => Int -> Simulation m field k (V.Vector field)
simpleSimulation !n = do
  metropolisSequence n 
  (Lattice v) <- view (lattice.dataVector)
  V.freeze v

  {-
setTemperature :: Double -> Simulation m field k ()
setTemperature !kT =
  local $
  \ps ->
     ps
     { _metropolis =
       (metropolis ps)
       { _acceptanceCriteria = bolzmannAccept kT
       }
     }

setLattice :: LatticeData st field -> Simulation m field k ()
setLattice ld = local $ \ps -> ps { _lattice = ld }
{-| Generate a random vector from a uniform distibrution on the
    2-sphere. -}
-}
randomSpherical :: PrimMonad m => Gen (PrimState m) -> m (V3 Double)
randomSpherical !g = do
  u <- uniform g
  v <- uniform g
  let t = acos $ 2 * u - 1
      p = 2 * pi * v
  return $ V3 (cos p * sin t) (sin p * sin t) (cos t)
